package anangkur.com.testmodulkotlin.adapter

import anangkur.com.testmodulkotlin.R
import anangkur.com.testmodulkotlin.model.Item
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_list.view.*

class ItemAdapter (private val context: Context, private val itemList: List<Item>): RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_list, p0, false))
    }

    override fun getItemCount(): Int {
        return  itemList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItems(itemList.get(p1))
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bindItems(item: Item){
            itemView.txt_name.setText(item.name)
            Glide.with(itemView.context).load(item.image).into(itemView.img_image)
        }


    }
}
