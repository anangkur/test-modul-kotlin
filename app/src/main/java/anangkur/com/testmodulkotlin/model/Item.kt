package anangkur.com.testmodulkotlin.model

data class Item (val name: String?, val image: Int?)