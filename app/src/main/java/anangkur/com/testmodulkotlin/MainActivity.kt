package anangkur.com.testmodulkotlin

import anangkur.com.testmodulkotlin.R.array.club_image
import anangkur.com.testmodulkotlin.R.array.club_name
import anangkur.com.testmodulkotlin.adapter.ItemAdapter
import anangkur.com.testmodulkotlin.model.Item
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val itemList: MutableList<Item> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initData()

        recyclerview.layoutManager = LinearLayoutManager(this)
        recyclerview.adapter = ItemAdapter(this, itemList)
    }

    private fun initData(){
        val name = resources.getStringArray(club_name)
        val image = resources.obtainTypedArray(club_image)
        itemList.clear()
        for (i in name.indices){
            itemList.add(Item((name[i]), image.getResourceId(i,0)))
        }
        image.recycle()
    }
}
